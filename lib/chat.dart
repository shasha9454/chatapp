import 'package:chat_app/chatMessage.dart';
import 'package:chat_app/newMessage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

// ignore: camel_case_types
class chat extends StatefulWidget {
  const chat({super.key});

  @override
  State<chat> createState() => _chatState();
}

// ignore: camel_case_types
class _chatState extends State<chat> {
  void pushNotificaton() async {
    final fcm = FirebaseMessaging.instance;
    fcm.requestPermission();

    final token = await fcm.getToken();
    print(token);
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      pushNotificaton();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter chatApp'),
        actions: [
          IconButton(
              onPressed: () {
                FirebaseAuth.instance.signOut();
              },
              icon: const Icon(Icons.exit_to_app))
        ],
      ),
      body: const Column(
        children: [Expanded(child: chatMessage()), newMessage()],
      ),
    );
  }
}
