// ignore: file_names
import 'package:chat_app/message_bubble.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

// ignore: camel_case_types
class chatMessage extends StatelessWidget {
  const chatMessage({super.key});
  @override
  Widget build(BuildContext context) {
    // ignore: non_constant_identifier_names
    final AuthenticatedUser = FirebaseAuth.instance.currentUser!;
    return StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('chat')
            .orderBy('createAt', descending: true)
            .snapshots(),
        builder: (ctx, chatSnapShot) {
          if (!chatSnapShot.hasData || chatSnapShot.data!.docs.isEmpty) {
            return const Center(
              child: Text('Value is missing'),
            );
          }
          if (chatSnapShot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          }
          if (chatSnapShot.hasError) {
            return const Text('something went wrong!');
          }
          final LoadingMessage = chatSnapShot.data!.docs;

          return ListView.builder(
              reverse: true,
              itemCount: LoadingMessage.length,
              itemBuilder: (ctx, index) {
                final ChatMessage = LoadingMessage[index].data();
                final nextChatMessage = index + 1 < LoadingMessage.length
                    ? LoadingMessage[index + 1].data()
                    : null;

                final CurrentUserId = ChatMessage['userId'];
                final NextUserId =
                    nextChatMessage != null ? nextChatMessage['userIId'] : null;

                final nextUserId = CurrentUserId == NextUserId;

                if (nextUserId) {
                  return MessageBubble.next(
                      message: ChatMessage['text'],
                      isMe: AuthenticatedUser.uid == CurrentUserId);
                } else {
                  return MessageBubble.first(
                      userImage: ChatMessage['userImage'],
                      username: ChatMessage['username'],
                      message: ChatMessage['text'],
                      isMe: AuthenticatedUser.uid == CurrentUserId);
                }

                // return Text(LoadingMessage[index].data()['text']);
              });
        }
        //
        // if (!chatSnapShot.hasData || chatSnapShot.data!.docs.isEmpty) {
        //   return const Center(
        //     child: Text('No chats Available'),
        //   );
        // }
        //
        // // ignore: non_constant_identifier_names
        // return ListView.builder(
        //     reverse: true,
        //     itemCount: LoadingMessage.length,
        //     // ignore: body_might_complete_normally_nullable
        //     itemBuilder: (ctx, index) {
        //       final ChatMessage = LoadingMessage[index].data();

        //       final nextChatMessage = index + 1 < LoadingMessage.length
        //           ? LoadingMessage[index + 1].data()
        //           : null;
        //       final currentChatMessageuserId = ChatMessage['userId'];
        //       final nextCurrentMessageUserId =
        //           nextChatMessage != null ? nextChatMessage['userId'] : null;

        //       final nextUserisSame =
        //           currentChatMessageuserId == nextCurrentMessageUserId;

        //       if (nextUserisSame) {
        //         MessageBubble.next(
        //             message: ChatMessage['text'],
        //             isMe: AuthenticatedUser.uid == currentChatMessageuserId);
        //       } else {
        //         return MessageBubble.first(
        //             userImage: ChatMessage['userImage'],
        //             username: ChatMessage['username'],
        //             message: ChatMessage['text'],
        //             isMe: AuthenticatedUser.uid == currentChatMessageuserId);
        //       }
        //     });
        );
  }
}
