// ignore: file_names
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

// ignore: camel_case_types
class userImagePicker extends StatefulWidget {
  const userImagePicker({super.key, required this.onpicked});
  final void Function(File image) onpicked;
  @override
  State<StatefulWidget> createState() {
    return _userImagePickerState();
  }
}

// ignore: camel_case_types
class _userImagePickerState extends State<userImagePicker> {
  File? pickedImage;
  void _onSelect() async {
    final imagePicker = ImagePicker();
    final pickImage = await imagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50, maxWidth: 150);
    setState(() {
      pickedImage = File(pickImage!.path);
    });
    widget.onpicked(pickedImage!);
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      const Padding(padding: EdgeInsets.all(8)),
      CircleAvatar(
          radius: 40,
          backgroundColor: Colors.grey,
          foregroundImage:
              pickedImage != null ? FileImage(pickedImage!) : null),
      TextButton.icon(
          onPressed: _onSelect,
          icon: const Icon(Icons.image),
          label: Text('Add Image',
              style: TextStyle(color: Theme.of(context).colorScheme.primary)))
    ]);
  }
}
