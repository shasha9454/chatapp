import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class newMessage extends StatefulWidget {
  const newMessage({super.key});
  @override
  State<StatefulWidget> createState() {
    return _newMessageState();
  }
}

// ignore: camel_case_types
class _newMessageState extends State<newMessage> {
  var messageController = TextEditingController();

  @override
  void dispose() {
    messageController.dispose();
    super.dispose();
  }

  void submit() async {
    final entermessage = messageController.text;

    if (entermessage.trim().isEmpty) {
      return;
    }
    final user = FirebaseAuth.instance.currentUser!;
    final userData = await FirebaseFirestore.instance
        .collection('users')
        .doc(user.uid)
        .get();

    FirebaseFirestore.instance.collection('chat').add({
      'text': entermessage,
      'createAt': Timestamp.now(),
      'userId': user.uid,
      'username': userData.data()!['username'],
      'userImage': userData.data()!['image-url'],
    });

    messageController.clear();
    FocusScope.of(context).unfocus();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 24, right: 12, bottom: 15),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              autocorrect: true,
              controller: messageController,
              textCapitalization: TextCapitalization.sentences,
              decoration: const InputDecoration(
                  label: Text(
                'send a message',
                style: TextStyle(fontSize: 18),
              )),
            ),
          ),
          IconButton(
              onPressed: submit,
              color: Theme.of(context).colorScheme.primary,
              icon: const Icon(Icons.send)),
        ],
      ),
    );
  }
}
