import 'dart:io';

import 'package:chat_app/userImagePicker.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

final firebase = FirebaseAuth.instance;

// ignore: camel_case_types
class chat_screen extends StatefulWidget {
  const chat_screen({super.key});
  @override
  State<StatefulWidget> createState() {
    return _chat_screenState();
  }
}

// ignore: camel_case_types
class _chat_screenState extends State<chat_screen> {
  // ignore: unused_field
  File? _selecetedImage;

  // ignore: override_on_non_overriding_member
  var islogin = true;
  var _isAuthentication = false;

  @override
  Widget build(BuildContext context) {
    var useKey = GlobalKey<FormState>();
    var enteredValue = '';
    var enteredPassword = '';
    var enterUsername = '';
    void submit() async {
      var isvalid = useKey.currentState!.validate();
      if (!isvalid || !islogin && _selecetedImage == null) {
        return;
      }
      useKey.currentState!.save();
      try {
        setState(() {
          _isAuthentication = true;
        });

        if (islogin) {
          // ignore: unused_local_variable
          final userCredential = await firebase.signInWithEmailAndPassword(
              email: enteredValue, password: enteredPassword);
        } else {
          // ignore: unused_local_variable
          final userCredential = await firebase.createUserWithEmailAndPassword(
              email: enteredValue, password: enteredPassword);

          // ignore: non_constant_identifier_names
          final Storageref = FirebaseStorage.instance
              .ref()
              .child('user_Places')
              .child('${userCredential.user!.uid}.jpg');

          await Storageref.putFile(_selecetedImage!);

          final imageUrl = await Storageref.getDownloadURL();

          await FirebaseFirestore.instance
              .collection('users')
              .doc(userCredential.user!.uid)
              .set({
            'username': enterUsername,
            'email': enteredValue,
            'image-url': imageUrl,
          });
        }
      } on FirebaseAuthException catch (error) {
        if (error.code == 'email-already-in-use:') {
          // .. log in user
        }
        ScaffoldMessenger.of(context).clearSnackBars();
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(error.message ?? 'Authentication Falied')));
        setState(() {
          _isAuthentication = false;
        });
      }
    }

    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.primary,
      appBar: AppBar(
        title: const Text('Chat App'),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.only(
                    top: 30, left: 20, bottom: 20, right: 20),
                width: 200,
                child: Image.asset('lib/assets/images/chat.png'),
              ),
              Card(
                margin: const EdgeInsets.all(16),
                child: Form(
                    key: useKey,
                    child: Column(
                      children: [
                        userImagePicker(
                          onpicked: (pick) {
                            _selecetedImage = pick;
                          },
                        ),
                        if (!islogin)
                          TextFormField(
                            enableSuggestions: false,
                            decoration: const InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(left: 5, top: 5),
                                label: Text('Username')),
                            validator: (value) {
                              if (value == null ||
                                  value.isEmpty ||
                                  value.length < 4) {
                                return 'The length should be more than 4 ';
                              }
                              return null;
                            },
                            onSaved: (newValue) {
                              enterUsername = newValue!;
                            },
                          ),
                        TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          autocorrect: false,
                          textCapitalization: TextCapitalization.none,
                          validator: (value) {
                            if (value == null ||
                                value.trim().isEmpty ||
                                !value.contains('@')) {
                              return 'Please enter a valid email address';
                            }
                            return null;
                          },
                          onSaved: (newValue) {
                            enteredValue = newValue!;
                          },
                          decoration: const InputDecoration(
                              contentPadding: EdgeInsets.only(left: 5, top: 5),
                              label: Text('Email_Address')),
                        ),
                        TextFormField(
                          obscureText: true,
                          decoration: const InputDecoration(
                            contentPadding: EdgeInsets.only(left: 5, top: 5),
                            label: Text('password'),
                          ),
                          validator: (value) {
                            if (value == null ||
                                value.trim().isEmpty ||
                                value.length < 6) {
                              return 'password should be more than 6 character long ';
                            }
                            return null;
                          },
                          onSaved: (newValue) {
                            enteredPassword = newValue!;
                          },
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        if (_isAuthentication)
                          const CircularProgressIndicator(),
                        if (!_isAuthentication)
                          ElevatedButton(
                            onPressed: submit,
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Theme.of(context)
                                    .colorScheme
                                    .primaryContainer),
                            child: Text(
                              islogin ? 'Login' : 'SignUp',
                            ),
                          ),
                        const SizedBox(
                          height: 4,
                        ),
                        if (!_isAuthentication)
                          TextButton(
                              onPressed: () {
                                setState(() {
                                  islogin = !islogin;
                                });
                              },
                              style: TextButton.styleFrom(
                                  backgroundColor: Theme.of(context)
                                      .colorScheme
                                      .inversePrimary),
                              child: Text(islogin
                                  ? 'Create an account'
                                  : 'Already have an account'))
                      ],
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
